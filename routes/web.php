<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TorrentController@index')->name('torrent.home');
Route::get('/search', 'TorrentController@search')->name('torrent.search');
Route::get('/torrent/{id}', 'TorrentController@getDetails')->name('torrent.details');
Route::get('/update/{id}', 'TorrentController@update')->name('torrent.update');
Route::get('/top100', 'TorrentController@getTop100')->name('torrent.top100');
Route::get('/about', 'TorrentController@aboutUs')->name('torrent.about');
Route::get('/donate', 'TorrentController@donateUs')->name('torrent.donate');
Route::get('/privacy', 'TorrentController@privacy')->name('torrent.privacy');
Route::get('/dmca', 'TorrentController@dmca')->name('torrent.dmca');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
