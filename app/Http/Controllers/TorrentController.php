<?php

namespace App\Http\Controllers;

use App\helpers\Helpers;
use App\helpers\Imdb;
use App\helpers\TorInfo;
use App\helpers\TorUpdate;
use App\Torrent;
use Carbon\Carbon;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class TorrentController extends Controller
{

    public function index()
    {
        $temp = [];

        $helper = new Helpers();
        $userInfo = $helper->getUserInfo();

        $temp['index'] = self::getTotalIndex();
        $temp['ipaddress'] = $userInfo->ipaddress;
        $temp['city'] = $userInfo->city;
        $temp['country'] = $userInfo->country;

        // dd($temp);

        return view('index', compact('temp'));
    }

    public function search(Request $request)
    {
        $client = ClientBuilder::create()->build();
        $query = trim($request->input("query"));

        if ($query != '' && ctype_xdigit($query) && strlen($query) == 40) {
            $hash = strtolower($query);
            $resultIndex = $client->get([
                'index' => 'torrents',
                'type' => 'hash',
                'id' => $hash
            ]);

            $result = [
                'hits' => [
                    'total' => isset($resultIndex) ? 1 : 0,
                    'hits' => [$resultIndex]
                ]
            ];
//            dd($result);
        } else {
            $orderBy = $request->input("sort");
            $page = $request->get("page", 1);
            $per_page = $request->get('limit', 20);


            $from = ($page - 1) * $per_page;

            $params = array();
//        $params['scroll'] = '30s';
            $params['index'] = 'torrents';
            $params['type'] = 'hash';
            $params['size'] = $per_page;
            $params['from'] = $from;
            $params['body'] = [];
            if ($query != '') {
                $queryDoc = [
                    'function_score' => [
                        'query' => [
                            'bool' => [
                                'must' => [
                                    'match' => [
                                        'search' => [
                                            'query' => $query,
                                            'operator' => 'and'
                                        ]
                                    ]
                                ],
                                'must_not' => [[
                                    'term' => [
                                        'inactive' => true
                                    ]], [
                                    'range' => [
                                        'flags' => [
                                            'gte' => 5
                                        ]
                                    ]
                                ]]
                            ]
                        ],
                        'field_value_factor' => [
                            'field' => 'seeders',
                            'modifier' => 'sqrt',
                            'factor' => .1,
                            'missing' => 0
                        ],
                        'boost_mode' => 'sum',
                        'max_boost' => 10
                    ]
                ];
            } else {
                $queryDoc = [
                    'bool' => [
                        'must_not' => [[
                            'term' => [
                                'inactive' => true
                            ]], [
                            'range' => [
                                'flags' => [
                                    'gte' => 5
                                ]
                            ]
                        ]]
                    ]
                ];
                $sort = [
                    'seeders' => [
                        'order' => 'desc',
                        'missing' => '_last'
                    ]
                ];
            }

            if (isset($orderBy) && $orderBy != '' && in_array($orderBy, ['seeders', 'leechers', 'name', 'created'])) {
                $sort = [
                    $orderBy => [
                        'order' => 'desc',
                        'missing' => '_last'
                    ]
                ];
            } else {
                $sort = [
                    'seeders' => [
                        'order' => 'desc',
                        'missing' => '_last'
                    ]
                ];
            }

            if (isset($sort)) {
                $params['body']['sort'] = $sort;
            }
            if (isset($queryDoc)) {
                $params['body']['query'] = $queryDoc;
            }

            $result = $client->search($params);
        }

        $index = self::getTotalIndex();

        return view('search-result', compact('result', 'index', 'query', 'orderBy', 'page'));
    }

    public function getDetails(Request $request)
    {
        $id = $request->route("id");
        $client = ClientBuilder::create()->build();
        $paramsDetails = array();
        $paramsDetails['index'] = 'torrents';
        $paramsDetails['id'] = $id;
        $paramsDetails['type'] = 'hash';
        $resultDetails = $client->get($paramsDetails);
//        dd($result);

        $result = self::getAllTemp($resultDetails['_source'], $client);

//        dd($result);

        $params = array();
        $params['size'] = 10;
        $params['index'] = 'torrents';
        $params['body']['query'] = [
            'match' => [
                'name' => $result['name']
            ]
        ];
//        dd($params);
        $searchList = $client->search($params);

        $index = self::getTotalIndex();

        return view('result-details', compact('result', 'index', 'searchList'));
    }

    public function update(Request $request)
    {
        $id = $request->route("id");
        $client = ClientBuilder::create()->build();

        $torUpdate = new TorUpdate($client, $id);
        $torUpdate->updateTorrent();

        return redirect()->back()->with(compact('resultUpdate'));
    }


    public function getTop100(Request $request)
    {
        $query = trim($request->input("query"));
        $orderBy = $request->input("sort");
        $page = $request->get("page", 1);
        $per_page = $request->get('limit', 100);

        $client = ClientBuilder::create()->build();

        $from = ($page - 1) * $per_page;

        $params = array();
//        $params['scroll'] = '30s';
        $params['index'] = 'torrents';
        $params['type'] = 'hash';
        $params['size'] = $per_page;
        $params['from'] = $from;
        $params['body'] = [];
        $queryDoc = [
            'bool' => [
                'must_not' => [[
                    'term' => [
                        'inactive' => true
                    ]], [
                    'range' => [
                        'flags' => [
                            'gte' => 5
                        ]
                    ]
                ]]
            ]
        ];
        $sort = [
            'seeders' => [
                'order' => 'desc',
                'missing' => '_last'
            ]
        ];

        if (isset($orderBy) && $orderBy != '' && in_array($orderBy, ['seeders', 'leechers', 'name', 'created'])) {
            $sort = [
                $orderBy => [
                    'order' => 'desc',
                    'missing' => '_last'
                ]
            ];
        } else {
            $sort = [
                'seeders' => [
                    'order' => 'desc',
                    'missing' => '_last'
                ]
            ];
        }

        if (isset($sort)) {
            $params['body']['sort'] = $sort;
        }
        if (isset($queryDoc)) {
            $params['body']['query'] = $queryDoc;
        }

//        dd($params);
        $result = $client->search($params);
    //    dd($result);

        $index = self::getTotalIndex();

        return view('search-result', compact('result', 'index', 'orderBy', 'page'));
    }

    public function aboutUs(Request $request)
    {
        return view('about-us');
    }

    public function donateUs(Request $request)
    {
        return view('donate');
    }

    public function privacy(Request $request)
    {
        return view('privacy');
    }

    public function dmca(Request $request)
    {
        return view('dmca');
    }

    public static function getTotalIndex()
    {
        $totalIndex = 0;
        if (Cache::has('totalIndex')) {
            $totalIndex = Cache::get('totalIndex', 0);
        } else {
            $client = ClientBuilder::create()->build();
            $params = array();
            $params['index'] = 'torrents';
            $params['type'] = 'hash';
            $params['body']['query'] = [
                "match_all" => [
                    'boost' => 1.2
                ]
            ];
            $result = $client->search($params);
            $totalIndex = $result['hits']['total'];
            Cache::put('totalIndex', $totalIndex, now()->addDays(1));
        }

        return $totalIndex;
    }

    public static function getAllTemp($temp, $client)
    {
        if(isset($temp['length'])){
            $temp['size'] = Helpers::formatBytes($temp['length']);
        }
        if(isset($temp['files'])){
            $temp['hasFiles'] = true;
            $temp['length'] = 0;
            foreach($temp['files'] as $key=>$file){
                if (is_numeric($temp['length']) && is_numeric($file['length'])) {
                    $temp['length'] += $file['length'];
                    $temp['files'][$key]['size'] = Helpers::formatBytes($file['length']);
                }
                $temp['files'][$key]['path'] = str_replace(',', '/', $file['path']);
            }
            $temp['size'] = Helpers::formatBytes($temp['length']);
        }else{
            $temp['files'] = [];
            $temp['files'][]['path'] = $temp['name'];
            if(isset($temp['size'])){
                $temp['files'][]['size'] = $temp['size'];
            }
            $temp['hasFiles'] = true;

        }

        $temp['filesCounter'] = count($temp['files']);
//        $temp['verified'] = ($temp['upvotes'] >= 0) ? true : false;
        if(isset($temp['created'])){
            $temp['created'] = Helpers::time_elapsed_string('@' . $temp['created']);//date("Y-m-d", $temp['created'])
        }
        $temp['lastupdated'] = Helpers::time_elapsed_string('@' . $temp['peers_updated']);
        if($temp['peers_updated'] == '0') {
            $temp['lastupdated'] = $temp['created'];
        }
        $new_str = str_replace(str_split('\\/:.*?"<>|'), ' ', $temp['name']);
        $removedata = array("And","the","in","The","  "," - "," + ","[","]","  "," 7 ");
        $new_str = str_replace($removedata,'',$new_str);
        $new_str = explode(" ", $new_str);
        $temp['keywords'] = $new_str;

//        include(app_path() . '/helpers/torinfo.php');
//        include_once 'imdb.class.php';

        $torInfo = new TorInfo($temp, $client);
        $torInfo->getData();
        $tdecs = $torInfo->tdecs;

        if(isset($tdecs)){
            $temp['tdecs'] = $tdecs;;
        }
        $subject = $temp['name'];
        preg_match('/^(\[.*?\]).*?/i', $subject, $matches);
        $subject = ltrim($subject);
        $pattern = "%(.*?)( [0-9]{1}x[0-9]{2}| s[0-9]{2}e[0-9]{2}|720p|480p|1080p|hdtv|dvdrip|xvid| cd[0-9]|dvdscr|bdrip|r5|r6|hdrip|hdtv|brrip|divx|[\{\(\[]?[0-9]{4}).*%";
        preg_match($pattern, strtolower($subject), $matches);
        if(isset($matches[2])) {
            $matches[2] = str_replace('(','',$matches[2]);
        }

        // check to see if tv serie is(can be) detected
        preg_match("/([Ss]?)([0-9]{1,2})([xXeE\.\-]?)([0-9]{1,2})/", strtolower($subject), $found);
        //var_dump($matches); var_dump($found);
        if(isset($matches[1])){
            $watchtv = $matches[1].'/'. (isset($found[2]) ? $found[2].'-' : '').(isset($found[4]) ? $found[4] : '');
            $watchtv = str_replace(' ', '-', $watchtv);

            // form the query search title here ....
            if(strlen($matches[2]) == 4) {
                $q = $matches[1].$matches[2];
            } elseif (isset($found[0]) && strlen($found[0]) == 6) { // detect date in movie tittle
                $q = $matches[1].' tv';
                $temp['tvseason'] = 1;
            } // else this is a tv series so add tv in the search query.

            $q =$matches[1];

            $q = str_replace('%20', '', $q);
            $q = str_replace('%', '', $q);
            $q = str_replace(' ', '', $q);
            $q = str_replace('deadpool2', 'deadpool 2', $q);
            $IMDB = new Imdb($q);

            if ($IMDB->isReady) {
                $imdbid = substr($IMDB->getUrl(), -10);
                $imdbid = rtrim($imdbid,'/');
                $temp['awards'] = $IMDB->getAwards();
                $temp['imdbyear'] = $IMDB->getYear();
                $temp['userreview'] = $IMDB->getUserReview();
                $temp['title'] = $IMDB->getTitle($bForceLocal = false);
                $temp['language'] = $IMDB->getLanguage();
                if ($temp['language'] == 'n/A'){
                    $temp['language'] = '';
                }
                $temp['rating'] = $IMDB->getRating();
                if ($temp['rating'] == 'n/A'){
                    $temp['rating'] = '';
                }
                $temp['castimg'] = $IMDB->getCastImages();
                $temp['lol'] = $IMDB->getDescription();
                $temp['tvlink'] = $watchtv;
                $temp['genre'] = $IMDB->getGenre();
                if ($temp['genre'] == 'n/A'){
                    $temp['genre'] = '';
                }
                $temp['plot'] = $IMDB->getPlot($iLimit = 0);
                $temp['cast'] = $IMDB->getCast($iLimit = 5, $bMore = true);
                if ($temp['cast'] == 'n/A'){
                    $temp['cast'] = '';
                }
                $temp['director'] = $IMDB->getDirector();
                $temp['writer'] = $IMDB->getWriter();
                $temp['releasedate'] = $IMDB->getReleaseDate();
                if ($temp['releasedate'] == 'n/A'){
                    $temp['releasedate'] = '';
                }
                $temp['runtime'] = $IMDB->getRuntime();
                if ($temp['runtime'] == 'n/A'){
                    $temp['runtime'] = '';
                }
                $temp['country'] = $IMDB->getCountry();
                //$temp['poster'] = $IMDB->getPoster($sSize = 'small', $bDownload = false);
                $temp['imdbposter'] = substr($IMDB->getPoster($sSize = 'small', $bDownload = false), -31);
                if ($temp['imdbposter'] != 'imdb_fb_logo._CB1542065250_.png'){
                    $temp['poster'] = $IMDB->getPoster($sSize = 'small', $bDownload = false);
                }
                $temp['imdbid'] = $imdbid;
                $temp['imdblink'] = $IMDB->getUrl();
                $trailer = $IMDB->getTrailerAsUrl($bEmbed = false);
                $trailer = str_replace('video/imdb', 'videoembed', $trailer);
                if ($trailer != 'n/A'){
                    $temp['trailer'] = $trailer;
                }
                if ($temp['lol'] == 'n/A'){
                    $temp['lol'] = '';
                }
            }
        }
        if($temp['peers_updated'] > time() - (10*30)) {
            $temp['disabled'] = true;
        }
        $helper = new Helpers();
        $userInfo = $helper->getUserInfo();
//        dd($userInfo);
        $temp['ipaddress'] = $userInfo->ipaddress;
        $temp['city'] = $userInfo->city;
        $temp['country'] = $userInfo->country;

        return $temp;
    }
}
