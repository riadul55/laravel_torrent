<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

class Torrent extends Model
{
    use ElasticquentTrait;
    protected $fillable = ['dht', 'infohash', 'magnet', 'peers_updated', 'search', 'type',
        'updated', 'categories_updated', 'numFiles', 'has_torrent', 'created', 'seeders',
        'leechers'];

    protected $mappingProperties = array(
        'torrents' => [
            'mappings' => [
                'hash' => [
                    'properties' => [
                        'categories' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'categories_updated' => [
                            'type' => 'long'
                        ],
                        'created' => [
                            'type' => 'long'
                        ],
                        'dht' => [
                            'type' => 'long'
                        ],
                        'dl' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'files' => [
                            'properties' => [
                                'length' => [
                                    'type' => 'long'
                                ],
                                'path' => [
                                    'type' => 'text',
                                    'fields' => [
                                        'keyword' => [
                                            'type' => 'keyword',
                                            'ignore_above' => 256
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'has_torrent' => [
                            'type' => 'long'
                        ],
                        'infohash' => [
                            'type' => 'text',
                            'fields' => [
                                'type' => 'keyword',
                                'ignore_above' => 256
                            ]
                        ],
                        'leechers' => [
                            'type' => 'long'
                        ],
                        'magnet' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'name' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'numFiles' => [
                            'type' => 'long'
                        ],
                        'peers_updated' => [
                            'type' => 'long'
                        ],
                        'search' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'seeders' => [
                            'type' => 'long'
                        ],
                        'size' => [
                            'type' => 'long'
                        ],
                        'tags' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'type' => [
                            'type' => 'text',
                            'fields' => [
                                'keyword' => [
                                    'type' => 'keyword',
                                    'ignore_above' => 256
                                ]
                            ]
                        ],
                        'updated' => [
                            'type' => 'long'
                        ]
                    ]
                ]
            ]
        ]
    );
}
