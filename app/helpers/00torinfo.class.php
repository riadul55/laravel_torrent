<?php
$infohash = $temp['infohash'];
$sCacheFile = base_path().'/infocache/cache/' . $infohash . '.cache';
if (is_readable($sCacheFile)) {

    $tdata = file_get_contents($sCacheFile);
//    $tdata = \Illuminate\Support\Facades\Cache::get($sCacheFile);

    $sortstart = strpos($tdata, '<div role="tabpanel" class="tab-pane active" id="description">');
    $sortend = strpos($tdata,'<div role="tabpanel" class="tab-pane file-content" id="files">', $sortstart);
    $tdecs = substr($tdata, $sortstart+62,($sortend-$sortstart-69));

} else if(!isset($temp['verified'])){
    $url = "https://1337x.to/search/$infohash/1/";

    $sUrl = runCurl($url);

    $notfound = strpos($sUrl, '404 Not Found');
    $noresult = strpos($sUrl, 'No results were returned. Please refine your search.');

    if (($notfound == FALSE) && ($noresult == FALSE) && ($sUrl != FALSE)){
        $sortstart = strpos($sUrl, "/torrent/")+9;
        $sortend = strpos($sUrl,">", $sortstart);
        $url = substr($sUrl, $sortstart,($sortend-$sortstart-2));
        $url = str_replace(' ', '', $url);
        $path = "https://1337x.to/torrent/$url/";

        $tdata = runCurl($path);

        $notfound = strpos($tdata, '404 Not Found');
        if (($notfound == FALSE)){
           $sCacheFile = base_path().'/infocache/cache/' . $infohash . '.cache';
            file_put_contents($sCacheFile, $tdata);
//            \Illuminate\Support\Facades\Cache::put($sCacheFile, $tdata, now()->addMonths(1));

            $sortstart = strpos($tdata, '<li> <strong>Category</strong>');
            $sortend = strpos($tdata,"</li>", $sortstart);
            $category = substr($tdata, $sortstart+37,($sortend-$sortstart-45));
            if ($category=='Movies'){
                $category ='movie';
            }
            if ($category=='Games'){
                $category ='games';
            }
            if ($category=='Anime'){
                $category ='anime';
            }
            if ($category=='TV'){
                $category ='show';
            }
            $sortstart = strpos($tdata, '<li> <strong>Type</strong>');
            $sortend = strpos($tdata,"</li>", $sortstart);
            $tags = substr($tdata, $sortstart+33,($sortend-$sortstart-41));

            if ($tags == 'PC Software'){
                $tags = 'PCSOFT';
            }
            if ($tags == 'PC Game'){
                $tags = 'PCGAME';
            }
            $removedata = array(" ","/",".","-",",");
            $tags = str_replace($removedata,'',$tags);

            $sortstart = strpos($tdata, '<li> <strong>Language</strong>');
            $sortend = strpos($tdata,"</li>", $sortstart);
            $language = substr($tdata, $sortstart+37,($sortend-$sortstart-45));


            $sortstart = strpos($tdata, '<a href="/user/');
            $sortend = strpos($tdata,">", $sortstart);
            $uploader = substr($tdata, $sortstart+15,($sortend-$sortstart-17));
            if ($uploader ==''){
                $uploader = "DHT";
            }
            $sortstart = strpos($tdata, '<div role="tabpanel" class="tab-pane active" id="description">');
            $sortend = strpos($tdata,'<div role="tabpanel" class="tab-pane file-content" id="files">', $sortstart);
            $tdecs = substr($tdata, $sortstart+62,($sortend-$sortstart-69));

            if ($category =='XXX' && $tags == 'Video') {
                $params = [
                    'index' => 'torrents',
                    'type' => 'hash',
                    'id' => $infohash,
                    'body' => [
                        'doc' => [
                            'categories' => $category,
                            'language' => $language,
                            'verified' => '1',
                            'verifiedby' => '1337x.to',
                            'uploadid' => $uploader
                        ]
                    ]
                ];
            } else {
                $params = [
                    'index' => 'torrents',
                    'type' => 'hash',
                    'id' => $infohash,
                    'body' => [
                        'doc' => [
                            'categories' => $category,
                            'tags' => $tags,
                            'language' => $language,
                            'verified' => '1',
                            'verifiedby' => '1337x.to',
                            'uploadid' => $uploader
                        ]
                    ]
                ];

            }
            try{
                $result = $client->update($params);
            }catch(Exception $error){
            }
        }
    } else if(isset($temp['verified']) && $temp['verified'] !='1'){
        $params = [
            'index' => 'torrents',
            'type' => 'hash',
            'id' => $infohash,
            'body' => [
            'doc' => [
                'verified' => '0',
                ]
            ]
        ];
        try{
            $result = $client->update($params);
        }catch(Exception $error){

        }
    }

}
function runCurl ($sUrl)
{
    $ch = curl_init($sUrl);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
    $aCurldata = curl_exec($ch);
    $aCurlInfo = curl_getinfo($ch);
    curl_close($ch);
    if (200 !== $aCurlInfo['http_code'] && 302 !== $aCurlInfo['http_code']) {
            echo '<pre><b>cURL returned wrong HTTP code “' . $aCurlInfo['http_code'] . '”, aborting.</b></pre>';
        return false;
    }
    return $aCurldata;
}
?>
