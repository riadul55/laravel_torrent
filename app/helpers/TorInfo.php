<?php


namespace App\helpers;

class TorInfo
{
    private $temp;

    private $infohash;

    private $client;

    private $path;

    private $sCacheFile;

    public $tdecs;

    public $category;

    public $tags;

    public $language;

    public $uploader;

    public $updateResult;

    /**
     * TorInfo constructor.
     */
    public function __construct($temp, $client)
    {
        $this->temp = $temp;
        $this->infohash = $temp['infohash'];
        $this->client = $client;
        $this->path = '/home/infocache/';
        // $this->path = base_path().'/infocache/';

        if ( ! is_writable($this->path) && ! mkdir($this->path)) {
            throw new \Exception('The directory “' . $this->path . ' isn’t writable.');
        }

        if ( ! function_exists('curl_init')) {
            throw new \Exception('You need to enable the PHP cURL extension.');
        }

        $this->sCacheFile = $this->path . $this->infohash . '.cache';
    }

    public function getData()
    {
        if (is_readable($this->sCacheFile)){
            $this->getCachedFile();
        } else if (!isset($this->temp['verified'])){
            $this->getFetchedFiles();
        }
    }

    private function getCachedFile()
    {
        $tdata = file_get_contents($this->sCacheFile);
        $sortstart = strpos($tdata, '<div role="tabpanel" class="tab-pane active" id="description">');
        $sortend = strpos($tdata,'<div role="tabpanel" class="tab-pane file-content" id="files">', $sortstart);
        $this->tdecs = substr($tdata, $sortstart+62,($sortend-$sortstart-69));
    }

    private function getFetchedFiles()
    {
        $url = "https://1337x.to/search/".$this->infohash."/1/";
        $sUrl = TorInfo::runCurl($url);
        $notfound = strpos($sUrl, '404 Not Found');
        $noresult = strpos($sUrl, 'No results were returned. Please refine your search.');

        if (($notfound == FALSE) && ($noresult == FALSE) && ($sUrl != FALSE)){
            $sortstart = strpos($sUrl, "/torrent/")+9;
            $sortend = strpos($sUrl,">", $sortstart);
            $url = substr($sUrl, $sortstart,($sortend-$sortstart-2));
            $url = str_replace(' ', '', $url);
            $path = "https://1337x.to/torrent/$url/";

            $tdata = TorInfo::runCurl($path);

            $notfound = strpos($tdata, '404 Not Found');
            if (($notfound == FALSE)){
                file_put_contents($this->sCacheFile, $tdata);

                $sortstart = strpos($tdata, '<li> <strong>Category</strong>');
                $sortend = strpos($tdata,"</li>", $sortstart);
                $this->category = substr($tdata, $sortstart+37,($sortend-$sortstart-45));
                if ($this->category =='Movies'){
                    $this->category ='movie';
                }
                if ($this->category =='Games'){
                    $this->category ='games';
                }
                if ($this->category =='Anime'){
                    $this->category ='anime';
                }
                if ($this->category=='TV'){
                    $this->category ='show';
                }
                $sortstart = strpos($tdata, '<li> <strong>Type</strong>');
                $sortend = strpos($tdata,"</li>", $sortstart);
                $this->tags = substr($tdata, $sortstart+33,($sortend-$sortstart-41));

                if ($this->tags == 'PC Software'){
                    $this->tags = 'PCSOFT';
                }
                if ($this->tags == 'PC Game'){
                    $this->tags = 'PCGAME';
                }
                $removedata = array(" ","/",".","-",",");
                $this->tags = str_replace($removedata,'',$this->tags);

                $sortstart = strpos($tdata, '<li> <strong>Language</strong>');
                $sortend = strpos($tdata,"</li>", $sortstart);
                $this->language = substr($tdata, $sortstart+37,($sortend-$sortstart-45));

                $sortstart = strpos($tdata, '<a href="/user/');
                $sortend = strpos($tdata,">", $sortstart);
                $this->uploader = substr($tdata, $sortstart+15,($sortend-$sortstart-17));
                if ($this->uploader ==''){
                    $this->uploader = "DHT";
                }
                $sortstart = strpos($tdata, '<div role="tabpanel" class="tab-pane active" id="description">');
                $sortend = strpos($tdata,'<div role="tabpanel" class="tab-pane file-content" id="files">', $sortstart);
                $this->tdecs = substr($tdata, $sortstart+62,($sortend-$sortstart-69));

                $this->updateTorrent();
            }
        } else if(isset($this->temp['verified']) && $this->temp['verified'] !='1'){
            $params = [
                'index' => 'torrents',
                'type' => 'hash',
                'id' => $this->infohash,
                'body' => [
                    'doc' => [
                        'verified' => 0,
                    ]
                ]
            ];
            $this->updateClient($params);
        }
    }

    private function updateTorrent()
    {
        if ($this->category =='XXX' && $this->tags == 'Video') {
            $params = [
                'index' => 'torrents',
                'type' => 'hash',
                'id' => $this->infohash,
                'body' => [
                    'doc' => [
                        'categories' => $this->category,
                        'language' => ($this->language == 'English') ? true : false,
                        'verified' => 1,
                        'verifiedby' => '1337x.to',
                        'uploadid' => $this->uploader
                    ]
                ]
            ];
        } else {
            $params = [
                'index' => 'torrents',
                'type' => 'hash',
                'id' => $this->infohash,
                'body' => [
                    'doc' => [
                        'categories' => $this->category,
                        'tags' => $this->tags,
                        'language' => ($this->language == 'English') ? true : false,
                        'verified' => 1,
                        'verifiedby' => '1337x.to',
                        'uploadid' => $this->uploader
                    ]
                ]
            ];
        }
//        dd($params);
        $this->updateClient($params);
    }

    private function updateClient($params)
    {
        try{
            $this->updateResult = $this->client->update($params);
        }catch(\Exception $error){
            dd($error);
        }
    }

    private static function runCurl($sUrl)
    {
        $ch = curl_init($sUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4");
        $aCurldata = curl_exec($ch);
        $aCurlInfo = curl_getinfo($ch);
        curl_close($ch);
        if (200 !== $aCurlInfo['http_code'] && 302 !== $aCurlInfo['http_code']) {
            return false;
        }
        return $aCurldata;
    }
}
