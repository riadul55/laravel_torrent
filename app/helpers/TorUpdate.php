<?php


namespace App\helpers;

class TorUpdate
{
    private $client;

    private $id;

    /**
     * TorUpdate constructor.
     */
    public function __construct($client, $id)
    {
        $this->client = $client;
        $this->id = $id;
    }

    public function updateTorrent(){
        $scraper = new Scraper();

        $tracker = array( 'udp://tracker.coppersurfer.tk:6969/announce' );

        $info = $scraper->scrape($this->id, $tracker);
        $info = array_shift($info);

        $params = [
            'index' => 'torrents',
            'type' => 'hash',
            'id' => $this->id,
            'body' => [
                'doc' => [
                    'peers_updated' => time(),
                    'seeders' => $info['seeders'],
                    'leechers' => $info['leechers']
                ]
            ]
        ];

        try{
            $this->client->update($params);
        }catch(\Exception $error){
        }
    }
}
