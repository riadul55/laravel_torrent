@extends('layouts.master')

@section('page_meta')
    <meta name="description" content="Download millions of torrents with TV series, movies, music, PC/Playstation/Wii/Xbox games and more at 7torrents.cc Visit now!">
    <meta name="keywords" content="online torrent search engine, torrent magnet search movies, bittorrent movies search engine, free movies torrent online, torrent magnet search engine, Free Torrent Search Engine">
@endsection

@section('page_title', 'Torrent Engine')

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2 my-5 text-center">
            <div class="logo">
                <a href="{{ route('torrent.home') }}"><img src="{{ asset('assets/img/hlogo.svg') }}" alt="7torrents" title="7torrents" width="356" height="71"></a>
            </div>
            <h1 class="my-3 fs-18 fw-500">Search Movies, TV Series, Musics, Ebooks from 7Torrent Search Engine</h1>
            <form id="hdTutoForm" class="hsearch m-0" method="GET" action="{{ route('torrent.search') }}">
                <input id="query" type="text" name="query" class="form-control w-100" placeholder="Indexing {{ isset($temp['index']) ? $temp['index'] : 0 }} torrents" autofocus="autofocus" minlength="3" required>
                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
            </form>
            <div class="card mt-4">
                <div class=card-body>
<h2 class="fw-500 fs-18 text-danger">Warning! Always use a VPN When Downloading Torrents! </h2>
                <div class="warning-note mb-20">Your leaked IP address <strong class="ip text-danger">{{ $temp['ipaddress'] }}</strong> reveals your location from <strong>{{ $temp['city'] }}</strong>, <strong class="country">{{ $temp['country'] }}</strong>. <br>Do NOT download any torrent before hiding your IP with a VPN. <br><strong>To Help We are Offering 6 Months Free VPN to All 7torrents User's</strong></div>
                <a href="http://track.ultravpn.com/5dee921c8cab5/click/7torrents" target="_blank" class="btn btn-label btn-danger mb-3"><label><i class="fa fa-warning"></i></label>Claim Free 6 Months</a>


</div>
                </div>
	</div>
        </div>
      </div>
@endsection
