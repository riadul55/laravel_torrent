<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="ROBOTS" content="INDEX, FOLLOW">
    @yield('page_meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="canonical" href="{{\Request::fullUrl()}}">
    
    <title>@yield('page_title', 'Torrent Engine')</title>
    {{-- css here --}}
    <link rel="stylesheet" href="{{ asset('assets/css/core.min.css') }}">
    @stack('header-styles')
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    @stack('header-scripts')
</head>
<body class="fs-15">
<div class="preloader">
    <div class="spinner-dots">
      <span class="dot1"></span>
      <span class="dot2"></span>
      <span class="dot3"></span>
    </div>
</div>

    @yield('base.content')
    <script src="{{ asset('assets/js/core.min.js') }}"></script>
    <script src="{{ asset('https://1337x.to/js/lazyload.min.js') }}"></script>
    <script src="{{ asset('assets/js/script.js') }}"></script>
    @stack('footer-styles')
    @stack('footer-scripts')
</body>
</html>
