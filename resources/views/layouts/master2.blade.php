@extends('layouts.base')

@section('base.content')

    {{-- header includes --}}
    @include('layouts.partials._header_search')

    
    @yield('content')
    
    @include('layouts.partials._footer')

    
@endsection