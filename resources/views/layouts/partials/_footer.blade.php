</div>
<footer class="site-footer pb-0">
        <div class="row">
          <div class="col-md-6">
            <p class="text-center text-md-left">©2019 <a href="/">7TORRENTS</a>. All rights reserved.</p>
          </div>
          <div class="col-md-6">
            <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
              <li class="nav-item">
                <a class="nav-link" href="/about">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/donate">Donate</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/dmca">DMCA</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/privacy">Privacy Policy</a>
              </li>
            </ul>
          </div>
        </div>
        <small>7TORRENTS (7torrents.cc) is not a tracker and doesn't store any content. 7torrents.cc is a search engine just like Google.com. 7torrents.cc only collects torrent metadata and magnet link for providing to its visitors</small>
      </footer>
</main>