<header class="topbar topbar-expand-lg topbar-secondary">
                    <div class="topbar-left">
                        <span class="topbar-btn topbar-menu-toggler"><i>&#9776;</i></span>
                        <div class="topbar-brand">
                            <a href="/"><img src="/assets/img/logo.svg" alt="" width="176" height="35"></a>                            
                        </div>
                        <form id="hdTutoForm" class="hsearch" method="GET" action="{{ route('torrent.search') }}">
                            <input id="querystr" type="text" name="query" class="form-control" placeholder="Indexing {{ isset($index) ? $index : 0 }} torrents" autofocus="autofocus" minlength="3" required>
                            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                    
                    <div class="topbar-right">
<nav class="topbar-navigation">
            
                <ul class="menu">
                   <li class="menu-item">
                        <a class="menu-link" href="/top100"><span class="title">Top 100</span></a>
                    </li>
                    @guest
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('login') }}"><i class="fa fa-lock"></i><span class="title">{{ __('Login') }}</span></a>
                    </li>
                    @if (Route::has('register'))
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('register') }}"><span class="title">{{ __('Register') }}</span></a>
                    </li>
                    @endif
                @endguest
                    
                </ul>
    
        </nav>
</div>
</header>
<main class="main-container">   
  <div class="main-content">
      
      
