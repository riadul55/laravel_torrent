<header class="topbar topbar-expand-lg topbar-secondary">
                    <div class="topbar-left">
                        <span class="topbar-btn topbar-menu-toggler"><i>&#9776;</i></span>
                        <div class="topbar-brand">
                                <a href="{{ route('torrent.home') }}"><img src="/assets/img/logo.svg" alt="7torrents Logo" title="7torrents Logo" width="176" height="35"></a>
                        </div>
                        
                    </div>
                    
                    <div class="topbar-right">
<nav class="topbar-navigation">
            <ul class="menu">
                   <li class="menu-item">
                        <a class="menu-link" href="/top100"><span class="title">Top 100</span></a>
                    </li>
                    @guest
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('login') }}"><i class="fa fa-lock"></i><span class="title">{{ __('Login') }}</span></a>
                    </li>
                    @if (Route::has('register'))
                    <li class="menu-item">
                        <a class="menu-link" href="{{ route('register') }}"><span class="title">{{ __('Register') }}</span></a>
                    </li>
                    @endif
                @endguest
                    
                </ul>
        </nav>
                </div>
        </header>
<main class="main-container">
  <div class="main-content">
