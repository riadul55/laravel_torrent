@extends('layouts.base')

@section('base.content')

    {{-- header includes --}}
    @include('layouts.partials._header')

    
    @yield('content')
   
    
    @include('layouts.partials._footer')

    
@endsection