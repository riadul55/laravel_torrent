@extends('layouts.master2')

@section('page_meta')
    <meta name="description" content="Download millions of torrents with TV series, movies, music, PC/Playstation/Wii/Xbox games and more at 7torrents.cc Visit now!">
    <meta name="keywords" content="online torrent search engine, torrent magnet search movies, bittorrent movies search engine, free movies torrent online, torrent magnet search engine, Free Torrent Search Engine">
@endsection

@section('page_title', 'Torrent Engine')

@section('content')
    <div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="about">
<h1>DMCA – 7Torrents Search Engine</h1>

<p id="remove"><b>Takedown Instructions</b></p>
<ol>
<li>Remember that this is just a search tool, your content is not hosted here. Please be polite. There is no point in threats.</li>
<li>Please only provide URLs containing ID values. (Ex: 7torrents.cc/torrent/<b>579qo3wbbyfqq36b8xwbfddqbv4l1mf8on</b>)</li>
<li>Takedowns that do not provide URLs with uniquely identifying ID values will take considerably more time to process!</li>
<li>Avoid sending search query URLs. List specific results you want removed.</li>
<li>Use your company/business email. Free mailboxes (AOL, Yahoo, Hotmail, Gmail, etc) will take more time to process and verify.</li>
<li>Google <a href="https://www.google.com/search?q=dmca+notice">DMCA Notice</a> if you never done this before.</li>
<li>Send your takedowns in plain text to <a href="mailto:7torrentscc@protonmail.com">7torrentscc@protonmail.com</a> only. Takedowns sent to ISPs or other addresses might not get filed properly.</li>
</ol>
</div>
		</div>
	</div>
</div>
@endsection
