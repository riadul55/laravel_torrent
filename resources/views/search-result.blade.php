@extends('layouts.master2')

@section('page_meta')
    <meta name="description" content="Download millions of torrents with TV series, movies, music, PC/Playstation/Wii/Xbox games and more at 7torrents.cc Visit now!">
    <meta name="keywords" content="online torrent search engine, torrent magnet search movies, bittorrent movies search engine, free movies torrent online, torrent magnet search engine, Free Torrent Search Engine">
@endsection

@section('page_title', 'Torrent Engine')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Results for <strong>{{ isset($query) ? $query : '' }} <span class="small"> ({{ isset($result) ? $result['hits']['total'] : "" }} results)</span></strong></h5>
                        <ul class="card-controls nav nav-primary nav-dotted nav-dot-separated">

                        <li class="nav-item">
                            <a class="nav-link" href="/search?query={{ isset($query) ? $query : '' }}&sort=time">Added Time</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/search?query={{ isset($query) ? $query : '' }}&sort=leechers">Leechers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/search?query={{ isset($query) ? $query : '' }}&sort=seeders">Seeders</a>
                        </li>
                        <li class="nav-item">
                            <strong>Order by : </strong>
                        </li>
                  </ul>

                    </div>
                    <div id="results" class="media-list media-list-hover media-list-divided">
                        <div class="sr-box-body">
                    <div class="search-items" id="post_data">
                        @if(isset($result) && isset($result['hits']) && isset($result['hits']['hits']))
                            @foreach($result['hits']['hits'] as $data)
                                <div class="media" onclick="location.href='/torrent/{{ isset($data['_id']) ? $data['_id'] : '' }}';">
                                    <a class="avatar avatar-lg"><img src="{{ asset('assets/img/flim.png') }}" alt=""/></a>

                                    <div class="media-body">
                                        <h5><a href="/torrent/{{ isset($data['_id']) ? $data['_id'] : '' }}">{{ isset($data['_source']['name']) ? $data['_source']['name'] : '' }}</a></h5>
                                        @php
                                                $fileLength = 0;
                                                if (isset($data['_source']['files'])){
                                                    foreach ($data['_source']['files'] as $files){
                                                        $fileLength += $files['length'];
                                                    }
                                                }
                                                if ($fileLength > 0){
                                                    $i = floor(log($fileLength) / log(1024));
                                                    $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                                                    $fileSize = sprintf('%.02F', $fileLength / pow(1024, $i)) * 1 . ' ' . $sizes[$i];
                                                }
                                            @endphp
                                            <small class="text-fader pr-10 br-1 border-light"><i class="fa fa-cloud pr-10"></i>Size : <strong class="text-success">{{ isset($fileSize) ? $fileSize : '0 MB' }}</strong></small>
                                            <small class="text-fader pr-10 br-1 border-light"><i class="fa fa-cloud pr-10"></i>Files : <strong class="text-success">{{ isset($data['_source']['numFiles']) ? $data['_source']['numFiles'] : 0 }}</strong></small>
                                            <small class="text-fader pl-10 pr-10 br-1 border-light">Seeders : <strong class="text-success">{{ isset($data['_source']['seeders']) ? $data['_source']['seeders'] : 0 }}</strong><i class="fa fa-level-up px-1 text-success"></i></small>
                                            <small class="text-fader pl-10 pr-10 br-1 border-light text-danger">Leechers : <strong class="text-danger">{{ isset($data['_source']['leechers']) ? $data['_source']['leechers'] : 0 }}</strong><i class="fa fa-level-down px-1 text-danger"></i></small>
                                            @if(isset($data['_source']['created']))
                                            <small class="text-fader pl-10 pr-10 br-1 border-light"><i class="fa fa-clock-o pr-10"></i>Added : <strong class="text-success">{{ \Carbon\Carbon::createFromDate(date('Y-m-d H:i:s', $data['_source']['created']))->diffForHumans() }}</strong></small>
                                            @endif
                                    </div>
                                           <div class="media-right">
                                               @if(isset($data['_source']['magnet']))
                                                <a href="{!! $data['_source']['magnet'] !!}" class="btn btn-square btn-outline btn-success"><i class="fa fa-magnet m-0" aria-hidden="true"></i></a>
                                               @else
                                               <a href="magnet:?xt=urn:btih:{{ $data['_source']['infohash'] }}&dn={{ $data['_source']['name'] }}&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Feddie4.nl%3A6969&tr=udp%3A%2F%2Ftracker.pirateparty.gr%3A6969&tr=udp%3A%2F%2Fopentrackr.org%3A1337&tr=udp%3A%2F%2Ftracker.zer0day.to%3A1337" class="btn btn-square btn-outline btn-success"><i class="fa fa-magnet m-0" aria-hidden="true"></i></a>
                                               @endif
                                               
                                                <a href="https://www.itorrents.org/torrent/{{ $data['_source']['infohash'] }}.torrent" class="btn btn-square btn-outline btn-danger"><i class="fa fa-download m-0" aria-hidden="true"></i></a>
                                           </div>


                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                        <div class="sr-box-footer">
                    @if(isset($page))
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            @if($page > 1)
                                <li class="page-item"><a class="page-link" href="/search?query={{ isset($query) ? $query : '' }}&page={{ $page -1 }}">Previous</a></li>
                            @else
                                <li class="page-item disabled"><a class="page-link" href="/search?query={{ isset($query) ? $query : '' }}&page={{ $page -1 }}">Previous</a></li>
                            @endif
                            @for($i=max(1, $page - 5);$i < min($page + 5, ceil($result['hits']['total']/10));$i++)
                                <li class="page-item"><a class="page-link" href="/search?query={{ isset($query) ? $query : '' }}&page={{ $i }}">{{ $i }}</a></li>
                            @endfor
                            @if($page < round($result['hits']['total']/10)-1)
                                <li class="page-item"><a class="page-link" href="/search?query={{ isset($query) ? $query : '' }}&page={{ $page +1 }}">Next</a></li>
                            @else
                                <li class="page-item disabled"><a class="page-link" href="/search?query={{ isset($query) ? $query : '' }}&page={{ $page +1 }}">Next</a></li>
                            @endif
                        </ul>
                    </nav>
                    @endif
                </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
@endsection
