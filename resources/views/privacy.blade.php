@extends('layouts.master2')

@section('page_meta')
    <meta name="description" content="Download millions of torrents with TV series, movies, music, PC/Playstation/Wii/Xbox games and more at 7torrents.cc Visit now!">
    <meta name="keywords" content="online torrent search engine, torrent magnet search movies, bittorrent movies search engine, free movies torrent online, torrent magnet search engine, Free Torrent Search Engine">
@endsection

@section('page_title', 'Torrent Engine')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2>Privacy Policy</h2>
            <p>We do not record IP addresses, email addresses or any other piece of information deemed as personal.  Usernames and passwords are considered anonymous user information as no requirement (other than security requirements) are placed on them.  Logging of users is recorded only through server requests and no third party applications.  We retain page requests, queries and browser information (User Agent String) to help improve the functionality of our site and debug problems.  All passwords are stored using secure encryption.</p>
            <p>Data will be retained for the life of the site or until user account deletion.  Upon user account delete, any user information (username, password, settings) will be removed from our system immediately.  If 7torrents.cc ever shuts down, we will delete all information about users from our database.</p>
        </div>
    </div>
</div>
@endsection
