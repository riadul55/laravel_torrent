@extends('layouts.master2')

@section('page_meta')
    <meta name="description" content="{{ isset($result['name']) ? $result['name'] : '' }}, Size : {{ isset($result['size']) ? $result['size'] : '0' }}, Magnet, Torrent, infohash : {{ isset($result['infohash']) ? $result['infohash'] : '' }}, Total Files : {{ isset($result['numFiles']) ? $result['numFiles'] : 0 }}">
    <meta name="keywords" content="{{ isset($result['keywords']) ? implode(", ", $result['keywords']) : '' }}">
@endsection

@section('page_title')
7torrents - {{ isset($result['name']) ? $result['name'] : '' }}
@endsection

@section('content')
    <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title fs-15">
                                <strong>{{ isset($result['name']) ? $result['name'] : '' }}</strong>
                            </h1>
                        </div>
                        <div class="card-body card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <p class="mb-1"><strong class="w-100">Added Time : </strong> {{ isset($result['created']) ? $result['created'] : '' }} </p>
                                    <p class="mb-1"><strong class="w-100">Last Update : </strong> {{ isset($result['lastupdated']) ? $result['lastupdated'] : '' }} @if(isset($result['infohash'])) <a href="/update/{{ isset($result['infohash']) ? $result['infohash'] : '' }}" @if(isset($result['disabled']) && $result['disabled'] == true) style="pointer-events: none" @endif alt="Update tracker" class="isDisabled ml-10" title="update tracker"> <span class="icon is-small"><i class="fa fa-refresh"></i></span> Update Tracker </a> </p>@endif
                                    <p class="mb-1"><strong class="w-100">Seeders : </strong><small class="text-fader pl-10"><strong class="text-success">{{ isset($result['seeders']) ? $result['seeders'] : '0' }}</strong><i class="fa fa-level-up px-1 text-success"></i></small> </p>
                                    <p class="mb-1"><strong class="w-100">Leechers : </strong> <small class="text-fader pl-10 text-danger"><strong class="text-danger">{{ isset($result['leechers']) ? $result['leechers'] : '0' }}</strong><i class="fa fa-level-down px-1 text-danger"></i></small> </p>
                                    <p class="mb-1"><strong class="w-100">Size : </strong> {{ isset($result['size']) ? $result['size'] : '0' }}</p>
                                    <p class="mb-1"><strong class="w-100">Total Files : </strong>{{ isset($result['numFiles']) ? $result['numFiles'] : 0 }}</p>
                                    <p class="mb-1"><strong class="w-100">infohash : </strong>{{ isset($result['infohash']) ? $result['infohash'] : '' }}</p>
                                    <div class="my-3">
                                        @if(isset($result['keywords']))
                                        @foreach($result['keywords'] as $data)
                                            <a href="/search?query={{$data}}" class="btn btn-xs btn-noclick btn-primary" rel="keywords">{{$data}}</a>
                                        @endforeach
                                        @endif
                                    </div>
                                    <div class="my-3">
                                        @if(isset($result['magnet']))
                                            <a title="{{ isset($result['name']) ? $result['name'] : '' }}" href="{!! $result['magnet'] !!}" class="btn btn-label mb-2 btn-primary"><label><i class="fa fa-magnet"></i></label>Magnet Download</a>
                                        @endif
                                            <a title="{{ isset($result['name']) ? $result['name'] : '' }}" href="https://www.itorrents.org/torrent/{{ $result['infohash'] }}.torrent" class="btn btn-label mb-2 btn-info"><label><i class="fa fa-download"></i></label> Torrent Download</a>
                                    </div>
                                </div>
                                @if(isset($result['poster']))
                                <div class="col-md-3">
                                    <div class="poster">
                                        <a class="trailplay" data-fancybox="" data-type="iframe" data-src="{{ isset($result['trailer']) ? $result['trailer'] : '' }}" data-provide="tooltip" data-placement="top" title="Play trailer" data-original-title="Play trailer" href="javascript:;"></a>
                                        <img class="" src="{{ isset($result['poster']) ? $result['poster'] : '' }}" width="100%" alt="{{ isset($result['name']) ? $result['name'] : '' }}" title="{{ isset($result['name']) ? $result['name'] : '' }}">
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-12">
                            <div class="card mt-4">
                                <div class="card-body bg-lightest text-center">
                                    <h3 class="fw-500 text-danger">Warning! Always use a VPN When Downloading Torrents! </h3>
                                    <div class="warning-note mb-20">Your leaked IP address <strong class="ip text-danger">{{ isset($result['ipaddress']) ? $result['ipaddress'] : '0.0.0.0' }}</strong> reveals your location from <strong>{{ isset($result['city']) ? $result['city'] : '' }}</strong>, <strong class="country">{{ isset($result['country']) ? $result['country'] : '' }}</strong>. <br>Do NOT download any torrent before hiding your IP with a VPN. <br><strong>To Help We are Offering 6 Months Free VPN to All 7torrents User's</strong></div>
                                    <a href="http://track.ultravpn.com/5dee921c8cab5/click/7torrents" target="_blank" class="btn btn-label btn-danger mb-3"><label><i class="fa fa-warning"></i></label>Claim Free 6 Months</a>
                                </div>
                            </div>
                            @if(isset($result['tdecs']))
                            <div class="description">
                                <h2 class="fs-18"><strong>Description :</strong></h2>
                                {!! isset($result['tdecs']) ? $result['tdecs'] : '' !!}
                            </div>
                            @endif
                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title fs-15"><strong>Torrent Files</strong></h1>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group">
                                        @if(isset($result) && isset($result['files']))
                                            @foreach($result['files'] as $data)
                                                <li class="list-group-item"> <span class="badge">{{ isset($data['size']) ? $data['size'] : '' }}</span> {{ isset($data['path']) ? $data['path'] : '' }}</li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title"><strong>Related Torrent</strong></h5>
                        </div>
                        <div class="card-body">
                            @if(isset($searchList))
                                @foreach($searchList['hits']['hits'] as $data)
                                    @if(isset($result['infohash']) && $result['infohash'] != $data['_id'])
                                        <h6><a href="/torrent/{{ isset($data['_id']) ? $data['_id'] : '' }}">{{ isset($data['_source']['name']) ? $data['_source']['name'] : '' }}</a></h6>
                                        <small class="text-fader pl-10 pr-10 br-1 border-light"><strong class="text-success">{{ isset($data['_source']['seeders']) ? $data['_source']['seeders'] : 0 }}</strong><i class="fa fa-level-up px-1 text-success"></i></small>
                                        <small class="text-fader pl-10 pr-10 br-1 border-light text-danger"><strong class="text-danger">{{ isset($data['_source']['leechers']) ? $data['_source']['leechers'] : 0 }}</strong><i class="fa fa-level-down px-1 text-danger"></i></small>
                                        @if(isset($data['_source']['created']))
                                            <small class="text-fader pl-10"><i class="fa fa-clock-o pr-10"></i>Added : <strong class="text-success">{{ \Carbon\Carbon::createFromDate(date('Y-m-d H:i:s', $data['_source']['created']))->diffForHumans() }}</strong></small>
                                        @endif
                                        <hr class="hr-sm">
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
