@extends('layouts.master2')

@section('page_meta')
    <meta name="description" content="Download millions of torrents with TV series, movies, music, PC/Playstation/Wii/Xbox games and more at 7torrents.cc Visit now!">
    <meta name="keywords" content="online torrent search engine, torrent magnet search movies, bittorrent movies search engine, free movies torrent online, torrent magnet search engine, Free Torrent Search Engine">
@endsection

@section('page_title', 'Torrent Engine')

@section('content')
    <div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h2>About 7torrents.cc</h2>
			<p>
				<strong>Welcome to 7torrents.cc</strong>  We aim to be the easiest and most straight-forward torrent search engine on the planet.
			</p>

			<p>
				We have always hated the annoying ads, the 15 fake download buttons and the excessive tracking that many torrent sites have so we decided to do something about it.  We believe that privacy and anonymity are important parts of your browsing experience and that sites should be fast, uncomplicated and easy to use.
			</p>
			<hr>
			<p>
				So here is why 7torrents.cc is different:
				<ul>
					<li>No third party ads, tracking or resources.  This ensures that no matter what, no one else knows what happens on this site</li>
					<li>HTTPS only.  We don't have an insecure page on us</li>
					<li>No logs or information that would de-anonymize our users (no recording of IP addresses, emails, etc)</li>
				</ul>
				In short, we do not track anything that would risk putting our users privacy or anonymity.  On top of that, our privacy policy is not ever release any information to any person or company to the best of our abilities.
			</p>
			<hr>
			<p>
				<strong>Technical Details</strong>: 7torrents.cc is a DHT Search Engine that scans the DHT network for active torrents and pulls in information and indexes the data to make it search-able by you.
			</p>
			<br>
                        <h3>Please Donate 7torrents</h3>
                        <p><strong>Bitcoin:</strong> bc1qfn37cpuuzff60cz506gq2am58t5dq4gw27sdep</p>
			<br>
			<br>
		</div>
	</div>
</div>
@endsection
